$('.slide-header-home').owlCarousel({
    loop: true,
    margin: 0,
    autoplay: true,
    autoplayTimeout: 993000,
    nav: false,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
})
$('.slide-product-detail').owlCarousel({
    loop: true,
    margin: 0,
    autoplay: true,
    autoplayTimeout: 993000,
    nav: true,
    navText: ['<img srcset="./img/arrow-left-gray.png 2x" alt="arrow-left" />', '<img srcset="./img/arrow-right-gray.png 2x" alt="arrow-right" />'],
    dots: false,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
})

$('.featured-product-slider').owlCarousel({
    loop: true,
    margin: 50,
    autoplay: true,
    autoplayTimeout: 993000,
    dots: false,
    nav: true,
    navText: ['<img srcset="./img/arrow-left.png 2x" alt="arrow-left" />', '<img srcset="./img/arrow-right.png 2x" alt="arrow-right" />'],
    responsive: {
        0: {
            items: 1,
            margin: 20,
        },
        600: {
            items: 3,
            margin: 20,
        },
        1000: {
            items: 3
        }
    }
})

$(document).ready(function () {
    $(".header-nav-menu").click(function () {
        $(".wrapper").toggleClass("active-menu");
        $(".menu-respon").toggleClass("active");
    });

    // cart bar
    $(".header-block-cart").click(function () {
        $(".header-cart-block").addClass("active");
        $(".overlay-menu").addClass("active");
        // $(".wrapper").css({
        //     height: "100vh",
        //     overflow: "hidden"
        // })
    });

    // user bar
    $(".header-block-user").click(function () {
        $(".header-user-block").addClass("active");
        $(".overlay-menu").addClass("active");
        // $(".wrapper").css({
        //     height: "100vh",
        //     overflow: "hidden"
        // })
    });

    $(".overlay-menu, .header-cart-img, .header-user-img").click(function () {
        $(".header-cart-block").removeClass("active");
        $(".header-user-block").removeClass("active");
        $('.overlay-menu').removeClass("active");
        $(".wrapper").css({
            height: "unset",
            overflow: "unset"
        })
    });

    $('.header-nav-logo').click(function () {
        let urlRedirect = $(this).data("url");
        location.href = urlRedirect
    })

    // show hide password
    $("#password-current .icon-eye").click(function () {
        if ($('#password-current input').attr("type") == "text") {
            $('#password-current input').attr('type', 'password');
            $('#password-current i').addClass("fa-eye-slash");
            $('#password-current i').removeClass("fa-eye");
        } else if ($('#password-current input').attr("type") == "password") {
            $('#password-current input').attr('type', 'text');
            $('#password-current i').removeClass("fa-eye-slash");
            $('#password-current i').addClass("fa-eye");
        }
    });
    $("#password-new .icon-eye").on('click', function () {
        if ($('#password-new input').attr("type") == "text") {
            $('#password-new input').attr('type', 'password');
            $('#password-new i').addClass("fa-eye-slash");
            $('#password-new i').removeClass("fa-eye");
        } else if ($('#password-new input').attr("type") == "password") {
            $('#password-new input').attr('type', 'text');
            $('#password-new i').removeClass("fa-eye-slash");
            $('#password-new i').addClass("fa-eye");
        }
    });
    $(".password-input i").click(function () {
        if ($('.password-input input').attr("type") == "text") {
            $('.password-input input').attr('type', 'password');
            $('.password-input i').addClass("fa-eye-slash");
            $('.password-input i').removeClass("fa-eye");
        } else if ($('.password-input input').attr("type") == "password") {
            $('.password-input input').attr('type', 'text');
            $('.password-input i').removeClass("fa-eye-slash");
            $('.password-input i').addClass("fa-eye");
        }
    });

    $(".btn-update-pass").click(function () {
        $(".field-info-password > input").addClass("d-none");
        $(".change-password").addClass("d-block");
        $(".btn-update-pass").text("Lưu")
    });
});