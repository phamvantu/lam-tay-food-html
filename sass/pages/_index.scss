.intro {
  padding: 150px 0;
  background-image: url(" ./../img/bg-intro.png");
  background-repeat: no-repeat;
  background-position: 100% -160%;
  &-media {
    display: flex;
    align-items: center;
  }
  &-media-left {
    margin-right: 25px;
    img {
      margin-left: auto;
    }
    img:first-child {
      margin-bottom: 29px;
      display: block;
    }
  }
  &-media-right {
    position: relative;
    .story-our {
      position: absolute;
      left: 0;
      bottom: 0;
      z-index: 1;
      display: flex;
      justify-content: space-between;
      align-items: center;
      padding: 17px;
      width: 100%;
      &-test {
        color: $color-text-fourth;
        width: 110px;
        span {
          font-family: "Sacramento", cursive;
          font-size: 24px;
          line-height: 35px;
          letter-spacing: -0.05em;
        }
        p {
          font-weight: 600;
          font-size: 17px;
          line-height: 21px;
          letter-spacing: -0.05em;
          margin-bottom: 0;
        }
      }
    }
    &::after {
      content: "";
      position: absolute;
      top: 0;
      left: 0;
      background: linear-gradient(
        180deg,
        rgba(0, 29, 18, 0) 19.81%,
        #000000 68.18%
      );
      opacity: 0.9;
      border-radius: 5px;
      width: 100%;
      height: 100%;
    }
  }
  &-text {
    margin-top: 150px;
    margin-left: 72px;
    .slogan {
      padding-right: 50px;
    }
  }
  &-overlay {
    position: absolute;
    top: 0;
    right: 0;
  }
}

.featured-product {
  position: relative;
  background-color: $color-light-gray;
  padding: 150px 59px 150px 290px;
  &-inner {
    display: flex;
    justify-content: space-between;
  }
  &-text {
    width: 445px;
    .title-main {
      position: relative;
      z-index: 1;
      &::after {
        content: "";
        position: absolute;
        bottom: 0px;
        right: 35px;
        z-index: -1;
        width: 127px;
        height: 15px;
        background-color: $secondary-color;
      }
    }
    .intro-text {
      margin-top: 80px;
      margin-left: 0;
    }
    .slogan {
      padding-right: 0;
      margin-bottom: 25px;
    }
  }
  &-slider {
    width: 1000px !important;
    .owl-nav {
      position: absolute;
      top: 38%;
      width: 100%;
      transform: translateY(-50%);
      margin: 0;
      .owl-prev {
        left: -30px;
      }
      .owl-next {
        right: -30px;
      }
      .owl-prev,
      .owl-next {
        position: absolute;
        background: #fff !important;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.05);
        @include size(50px);
        border-radius: 50% !important;
        color: #230b34 !important;
        &:focus {
          outline: none;
        }
        &:hover {
          box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.05) !important;
        }
        img {
          margin: 0 auto;
        }
      }
    }
    .owl-dots {
      display: block !important;
      margin-top: 0 !important;
      position: relative;
      top: -20px;
      .owl-dot {
        &:focus {
          outline: none;
        }
        &.active,
        &:hover {
          span {
            background-color: $primary-color !important;
          }
        }
        span {
          @include size(8px);
        }
      }
    }
  }
  &-overlay {
    position: absolute;
    top: 70px;
    @include maxWidth(1400px) {
      width: 80%;
    }
  }
}

.owl-carousel.featured-product-slider .owl-item img {
  width: unset !important;
}

.owl-theme .owl-nav {
  margin: 0 !important;
}

.care {
  &-product {
    padding: 150px 0;
  }
  &-product-text {
    display: flex;
    justify-content: space-between;
    align-items: flex-end;
    margin-bottom: 50px;
    .head-title {
      margin-bottom: 15px;
    }
    .title-main {
      margin-bottom: 0px;
    }
  }
  &-list {
    display: flex;
    justify-content: space-between;
  }
  &-item {
    position: relative;
    width: calc(33.33% - 32.6px);
    &:after {
      content: "";
      position: absolute;
      width: 100%;
      height: 100%;
      top: 0;
      left: 0;
      background: linear-gradient(
        180deg,
        rgba(0, 0, 0, 0) 27.54%,
        #000000 100%
      );
      border-radius: 5px;
    }
  }
  &-item-desc {
    position: absolute;
    bottom: 0;
    z-index: 1;
    display: flex;
    justify-content: space-between;
    width: 100%;
    padding: 0 20px 17px 21px;
    h3 {
      font-weight: 600;
      font-size: 30px;
      line-height: 37px;
      letter-spacing: -0.05em;
      color: $color-text-fourth;
      margin-bottom: 0;
    }
  }
  &-item-thumbnail {
    width: 100%;
    height: 100%;
    border-radius: 5px;
    object-fit: cover;
  }
  &-item-detail {
    width: 40px;
    height: 40px;
    display: flex;
    align-items: center;
    position: relative;
    top: -5px;
    img {
      margin: 0 auto;
    }
    &::after {
      content: "";
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background: #e0a612;
      opacity: 0.21;
      border-radius: 50%;
    }
  }
}

.register {
  background: $color-light-gray;
  padding: 98px 0 100px;
  text-align: center;
  position: relative;
  &-text {
    .title-main {
      margin-bottom: 12px;
    }
    .slogan {
      line-height: 30px;
      padding-right: 0;
      font-weight: 300;
      margin-bottom: 37px;
    }
  }
  &-form {
    input {
      background: #ffffff;
      border-radius: 2px;
      width: 480px;
      height: 50px;
      border: 0;
      color: $color-text-third;
      font-weight: 300;
      font-size: 17px;
      line-height: 30px;
      padding: 10px 18px;
      &::placeholder {
        color: $color-gray;
      }
    }
    button {
      display: block;
      margin: 20px auto 0;
    }
  }
  .register-overlay-left,
  .register-overlay-right {
    position: absolute;
    bottom: 0;
  }
  .register-overlay-left {
    left: 0;
  }
  .register-overlay-right {
    right: 0;
  }
}

.knowledge {
  padding: 151px 0 150px;
  &-list {
    display: flex;
    justify-content: space-between;
  }
  &-item {
    width: calc(25% - 38.25px);
    height: 350px;
    position: relative;
    &::before {
      content: "";
      position: absolute;
      width: 100%;
      height: 100%;
      top: 0;
      left: 0;
      background: linear-gradient(
        180deg,
        rgba(0, 0, 0, 0) 31.25%,
        #000000 94.57%
      );
      border-radius: 5px;
    }
    &:hover .see-more {
      opacity: 1;
      a {
        color: #fff;
      }
    }
    &:hover .knowledge-item-desc {
      transform: translateY(-8px);
    }
    &:hover {
      &::before {
        background: linear-gradient(
          180deg,
          rgba(0, 0, 0, 0.66) 4.17%,
          #000000 94.57%
        );
      }
    }
    &-thumbnail {
      object-fit: cover;
    }
  }
  &-item-desc {
    position: absolute;
    width: 100%;
    bottom: 0;
    color: $color-text-fourth;
    padding: 0 20px 20px;
    @include transition;
    transform: translateY(37px);
  }
  &-time {
    font-size: 13px;
    line-height: 16px;
    letter-spacing: -0.05em;
    text-transform: uppercase;
    margin-bottom: 10px;
    padding-left: 10px;
    border-left: 3px solid $secondary-color;
  }
  &-title {
    font-size: 18px;
    line-height: 22px;
    margin: 0;
    font-weight: 300;
    @include textClamp(3);
    .head-title {
      margin-bottom: 10px;
    }
  }
  .see-more {
    margin-top: 12px;
    @include transition;
    opacity: 0;
    img {
      padding-left: 5px;
    }
    a {
      // color: $color-text-fourth;
    }
  }
}

@include maxWidth(1800px) {
  .featured-product {
    padding-left: 150px;
  }
}

@include maxWidth(1500px) {
  .featured-product {
    padding-left: 80px;
    &-slider {
      width: 700px !important;
    }
  }
}

@include maxWidth(1200px) {
  .featured-product-inner {
    flex-wrap: wrap;
    &-text {
      width: 100%;
      .intro-text {
        margin: 30px 0;
      }
    }
    &-slider {
      width: 100% !important;
    }
  }
  .knowledge {
    padding: 151px 0 150px;
    &-list {
      flex-wrap: wrap;
    }
    &-item {
      width: calc(25% - 10px);
      height: 320px;
    }
  }
}

@include maxWidth(992px) {
  .intro {
    padding: 100px 0;
    &-media-left {
      margin-right: 15px;
      img {
        margin-left: auto;
      }
    }
    &-text {
      margin-top: 0px;
      margin-left: 0px;
      .slogan {
        padding-right: 0px;
      }
    }
    &-overlay {
      position: absolute;
      top: 0;
      right: 0;
    }
  }
  .knowledge {
    padding: 100px 0 100px;
    &-item {
      width: calc(50% - 20px);
      height: 320px;
      margin-bottom: 30px;
      &-thumbnail {
        height: 100%;
        width: 100%;
      }
    }
  }
}

@include maxWidth(768px) {
  .care {
    &-product {
      padding: 100px 0;
    }
    &-list {
      flex-wrap: wrap;
    }
    &-item {
      width: calc(33.33% - 10px);
    }
  }
  .register {
    z-index: 1;
    &-form {
      position: relative;
      z-index: 3;
    }
    .register-overlay-left,
    .register-overlay-right {
      z-index: 2;
    }
  }
  .featured-product {
    &-text {
      .title-main::after {
        left: 100px;
      }
    }
  }
}

@include maxWidth(576px) {
  .register {
    &-form {
      input {
        width: 100%;
      }
    }
  }
  .featured-product {
    padding: 30px 30px 40px;
  }
  .care {
    &-item {
      width: calc(100%);
      height: 240px;
      margin-bottom: 20px;
    }
  }
  .knowledge {
    padding: 50px 0;
    &-item {
      width: calc(50% - 10px);
      height: 300px;
    }
  }
  .intro {
    padding: 100px 0;
    &-text {
      margin-top: 50px;
    }
  }
  .featured-product {
    padding: 0 15px 50px;
    &-slider {
      width: 100% !important;
      .owl-nav {
        .owl-prev {
          left: 0px;
        }
        .owl-next {
          right: 0px;
        }
      }
    }
    &-overlay {
      width: 100%;
      display: none;
    }
  }
}

.fade-loading {
  border-radius: 50%;
  position: relative;
}
.fade-loading:before {
  content: "";
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  border-radius: inherit;
  border: 5px solid #e0a61285;
  animation: fade 1s forwards infinite linear;
}
@keyframes fade {
  to {
    transform: scale(1.6);
    opacity: 0;
  }
}
